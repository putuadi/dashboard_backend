<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function index()
    {

        $user = auth()->user();
        return response()->json($user);

    }

    public function update(Request $request)
    {
        $user = auth()->user();
        $user = User::where("email", $user->email)->first();
        //define validation rules
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
        ]);
        //check if validation fails
        if ($validator->fails()) {
            return response()->json([
                "message" => "Invalid field",
                "errors" => $validator->errors(),
            ], 422);
        }
        //update post with new image
        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->user),
        ]);
        return response()->json([
            "message" => "Update success",
            "user" => $user,
        ]);

    }

    public function destroy()
    {
        $user = auth()->user();
        $user = User::where("email", $user->email)->first();
        // delete account
        $user->delete();
        return response()->json([
            "message" => "Delete success",
        ]);
    }
}
