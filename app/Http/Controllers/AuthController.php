<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $validasi = Validator::make($request->all(), [
            "name" => "required",
            "email" => "required|email|unique:users",
            "password" => "required",
            "confirm_password" => "required|same:password",
        ]);
        if ($validasi->fails()) {
            return response()->json([
                "message" => "Invalid field",
                "errors" => $validasi->errors(),
            ], 422);
        }
        $user = User::create([
            "name" => $request->name,
            "email" => $request->email,
            "password" => bcrypt($request->password),
        ]);
        return response()->json([
            "message" => "Berhasil",
            "user" => $user,
        ], 201);
    }
    public function login(Request $request)
    {
        $validasi = Validator::make($request->all(), [
            "email" => "required|email",
            "password" => "required",
        ]);
        if ($validasi->fails()) {
            return response()->json([
                "message" => "Invalid field",
                "errors" => $validasi->errors(),
            ], 422);
        } else if (!Auth::attempt($validasi->validate())) {
            return response()->json([
                "message" => "Email or password incorect",
            ], 401);
        }

        $user = User::where("email", $request->email)->first();
        $token = $user->createToken("user_login")->plainTextToken;
        return response()->json([
            "message" => "Login success",
            "user" => [
                "name" => $user->name,
                "email" => $user->email,
                "accessToken" => $token,
            ],
        ], 200);
    }
    public function logout()
    {
        $user = Auth::user();
        $user->tokens()->delete();
        return response()->json([
            "message" => "Logout success",
        ], 200);
    }
}
