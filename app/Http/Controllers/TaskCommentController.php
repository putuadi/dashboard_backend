<?php

namespace App\Http\Controllers;

use App\Models\TaskComment;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskCommentController extends Controller
{
    public function index($task): JsonResponse
    {
        $comments = TaskComment::where("task_id", $task)->get();

        return response()->json([
            'status' => 'success',
            'message' => 'Data Komentar berhasil ditampilkan',
            'data' => $comments,
        ], 200);
    }

    public function store(Request $request, $task): JsonResponse
    {
        $request->validate([
            "comment" => "required",
        ]);

        $user = Auth::user();

        $comment = TaskComment::create([
            "task_id" => $task,
            "user_id" => $user->id,
            "comment" => $request->comment,
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'Komentar berhasil ditambahkan',
            'data' => $comment,
        ], 201);
    }

    public function update(Request $request, $task, $comment): JsonResponse
    {
        $comment = TaskComment::where("id", $comment)->where("task_id", $task)->first();
        $request->validate([
            "comment" => "required",
        ]);

        $comment->update([
            "comment" => $request->comment,
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'Komentar berhasil diperbarui',
            'data' => $comment,
        ], 200);
    }

    public function destroy($task, $comment): JsonResponse
    {
        $comment = TaskComment::where("id", $comment)->where("task_id", $task)->first();

        $comment->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Komentar berhasil dihapus',
        ], 200);
    }
}
