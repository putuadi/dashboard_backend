<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProjectRequest;
use App\Http\Requests\UpdateProjectRequest;
use App\Http\Resources\ProjectDetailResource;
use App\Http\Resources\ProjectResource;
use App\Models\Project;
use App\Models\ProjectRoles;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $project_roles = ProjectRoles::where('user_id', auth()->user()->id)->get();
        $project = Project::query();
        foreach ($project_roles->pluck('project_id') as $data) {
            $project = $project->orWhere('id', $data);
        }
        $project = $project->with(["getCreatedBy", "getTasks"])->get();
        return ProjectResource::collection($project);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreProjectRequest $request)
    {
        $project_create = Project::create([
            "title" => $request->title,
            "description" => $request->description,
            "content" => $request->content,
            "created_by" => Auth::user()->id,
        ]);
        ProjectRoles::create([
            "project_id" => $project_create->id,
            "user_id" => auth()->user()->id,
            "role" => "owner",
            "valid_until" => '2023-12-31 08:48:32',
        ]);
        return response()->json([
            "message" => "success store",
            "project" => $project_create,
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Project $project)
    {
        $project_detail = Project::with(["getCreatedBy", "getDeletedBy"])->find($project->id);
        return new ProjectDetailResource($project_detail);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateProjectRequest $request, Project $project)
    {
        if ($request->content == null) {
            $content = $project->content;
        } else {
            $content = $request->content;
        }

        $project->update([
            "title" => $request->title,
            "description" => $request->description,
            "content" => $content,
        ]);
        return response()->json([
            "message" => "success",
            "project update" => $project,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Project $project)
    {
        $project->update([
            "deleted_by" => Auth::user()->id,
        ]);
        $project->delete();
        return response()->json([
            "message" => "delete berhasil",
            "project delete" => $project,
        ]);
    }
}
