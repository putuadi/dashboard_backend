<?php

namespace App\Http\Controllers;

use App\Models\ProjectRoles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProjectRolesController extends Controller
{
    public function index($project)
    {
        // $project_roles = ProjectRoles::with(['getProjects', 'getUsers'])->where('project_id', $project)->get();
        $project_roles = ProjectRoles::where('project_id', $project)->get();
        return response()->json([
            "message" => "success",
            "data" => $project_roles,
        ], 200);
    }
    public function store(Request $request, $project)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'role' => 'required',
            'valid_until' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'failed',
                'errors' => $validator->errors(),
            ], 422);
        }
        $project_roles = ProjectRoles::create([
            "project_id" => $project,
            "user_id" => $request->user_id,
            "role" => $request->role,
            "valid_until" => $request->valid_until,
        ]);
        return response()->json([
            "message" => "success",
            "data" => $project_roles,
        ]);
    }
    public function update(Request $request, $project, $user)
    {
        $validator = Validator::make($request->all(), [
            "role" => "required",
        ]);
        if ($validator->fails()) {
            return response()->json([
                "message" => "failed",
                "errors" => $validator->errors(),
            ]);
        }
        $project_roles = ProjectRoles::where('project_id', $project)->where('user_id', $user)->first();
        if (!$project_roles) {
            return response()->json([
                "message" => "failed",
                "errors" => "data tidak ada",
            ]);
        }
        $project_roles->update([
            "role" => $request->role,
        ]);
        return response()->json([
            "message" => "success",
            "user" => $project_roles,
        ]);
    }
    public function destroy($project, $user)
    {
        $project_roles = ProjectRoles::where('project_id', $project)->where('user_id', $user)->first();
        $project_roles->delete();
        return response()->json([
            "message" => "success",
            "user" => $project_roles,
        ]);
    }
}
