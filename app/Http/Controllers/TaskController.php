<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Task;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Project $project): JsonResponse
    {
        $tasks = Task::where("project_id", $project->id)->get();

        return response()->json([
            'status' => 'success',
            'message' => 'Data Tugas berhasil ditampilkan',
            'data' => $tasks,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, Project $project): JsonResponse
    {
        $user = Auth::user();
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'completed' => 'required|boolean',
            'content' => 'nullable',
            'due_date' => 'nullable|date',
            'assign_to' => 'nullable|exists:users,id',
        ]);

        $task = Task::create([
            "title" => $request->title,
            "description" => $request->description,
            "completed" => $request->completed,
            "content" => $request->content,
            "created_by" => $user->id,
            "project_id" => $project->id,
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'Tugas berhasil ditambahkan',
            'data' => $task,
        ], 201);
    }

    /**
     * Display the specified resource.
     */
    public function show($project, $task): JsonResponse
    {
        $task = Task::where("id", $task)->where("project_id", $project)->first();
        return response()->json([
            'status' => 'success',
            'message' => 'Data Tugas berhasil ditampilkan',
            'data' => $task,
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $project, $task): JsonResponse
    {
        $task = Task::where("id", $task)->where("project_id", $project)->first();
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'completed' => 'required|boolean',
            'content' => 'nullable',
            'due_date' => 'nullable|date',
            'assign_to' => 'nullable|exists:users,id',
        ]);

        $task->update([
            "title" => $request->title,
            "description" => $request->description,
            "completed" => $request->completed,
            "content" => $request->content,
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'Tugas berhasil diperbarui',
            'data' => $task,
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($project, $task): JsonResponse
    {
        $task = Task::where("id", $task)->where("project_id", $project)->first();
        $task->update([
            "deleted_by" => Auth::user()->id,
        ]);
        $task->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'Tugas dihapus',
        ], 200);
    }
}
