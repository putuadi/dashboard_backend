<?php

namespace App\Http\Controllers;

use App\Models\TaskFile;
use App\Http\Requests\StoreTaskFileRequest;
use App\Http\Requests\UpdateTaskFileRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class TaskFileController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index($task): JsonResponse
    {
        $files = TaskFile::where("task_id", $task)->get();

        return response()->json([
            'status' => 'success',
            'message' => 'Data File tugas berhasil ditampilkan',
            'data' => $files,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request, $task): JsonResponse
    {
        $request->validate([
            "file" => "required|file|mimes:pdf,doc,docx,xls,xlsx,ppt,pptx,txt,jpg,jpeg,png,gif|max:2048",
            "description" => "nullable|string",
        ]);

        $user = Auth::user();

        $file = $request->file("file");
        $filePath = $file->store("task_files");

        $taskFile = TaskFile::create([
            "task_id" => $task,
            "file_name" => $file->getClientOriginalName(),
            "file_path" => $filePath,
            "description" => $request->description,
            "uploaded_by" => $user->id,
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'File tugas berhasil ditambahkan',
            'data' => $taskFile,
        ], 201);
    }

    /**
     * Display the specified resource.
     */
    public function show($task, $file): JsonResponse
    {
        $taskFile = TaskFile::where("id", $file)->where("task_id", $task)->first();
    
        if (!$taskFile) {
            return response()->json([
                'status' => 'error',
                'message' => 'File tugas tidak ditemukan',
            ], 404);
        }
    
        return response()->json([
            'status' => 'success',
            'message' => 'Data File tugas berhasil ditampilkan',
            'data' => $taskFile,
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $task, $file): JsonResponse
    {
        $taskFile = TaskFile::where("id", $file)->where("task_id", $task)->first();
        
        if (!$taskFile) {
            return response()->json([
                'status' => 'error',
                'message' => 'File tugas tidak ditemukan',
            ], 404);
        }

        $request->validate([
            "description" => "nullable|string",
        ]);

        $taskFile->update([
            "description" => $request->description,
        ]);

        return response()->json([
            'status' => 'success',
            'message' => 'Deskripsi file tugas berhasil diperbarui',
            'data' => $taskFile,
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($task, $file): JsonResponse
    {
        $taskFile = TaskFile::where("id", $file)->where("task_id", $task)->first();

        if (!$taskFile) {
            return response()->json([
                'status' => 'error',
                'message' => 'File tugas tidak ditemukan',
            ], 404);
        }

        Storage::delete($taskFile->file_path);
        $taskFile->delete();

        return response()->json([
            'status' => 'success',
            'message' => 'File tugas berhasil dihapus',
        ], 200);
    }
}
