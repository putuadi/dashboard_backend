<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = "projects";
    protected $primaryKey = "id";
    protected $guarded = ["id"];

    public function getCreatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, "created_by", "id");
    }
    public function getDeletedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, "deleted_by", "id");
    }
    public function getTasks(): HasMany
    {
        return $this->hasMany(Task::class, "project_id", "id");
    }
}
