<?php

namespace App\Models;

use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectRoles extends Model
{
    use HasFactory, SoftDeletes;
    public $table = 'project_roles';
    public $primaryKey = 'id';
    public $guarded = ['id'];

    public function getProjects(): BelongsTo
    {
        return $this->belongsTo(Project::class, 'project_id', 'id');
    }
    public function getUsers(): BelongsTo
    {
        return $this->belongsto(User::class, 'user_id', 'id');
    }
}
