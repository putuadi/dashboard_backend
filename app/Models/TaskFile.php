<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class TaskFile extends Model
{
    use HasFactory;

    protected $table = "task_files";
    protected $primaryKey = "id";
    protected $guarded = ["id"];

    public function task(): BelongsTo
    {
        return $this->belongsTo(Task::class, "task_id", "id");
    }
    public function uploader(): BelongsTo
    {
        return $this->belongsTo(User::class, "user_id", "id");
    }
}
