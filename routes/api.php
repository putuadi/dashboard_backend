<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\ProjectRolesController;
use App\Http\Controllers\TaskCommentController;
use App\Http\Controllers\TaskFileController;
use App\Http\Controllers\TaskController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
 */

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//Auth
Route::post("register", [AuthController::class, "register"]);
Route::post("login", [AuthController::class, "login"]);
Route::post("logout", [AuthController::class, "logout"])->middleware("auth:sanctum");

//Profile
Route::get("profile", [ProfileController::class, "index"])->middleware("auth:sanctum");
Route::post("profile/update", [ProfileController::class, "update"])->middleware("auth:sanctum");
Route::post("profile/delete", [ProfileController::class, "destroy"])->middleware("auth:sanctum");

//Projects
Route::resource('projects', ProjectController::class)->middleware("auth:sanctum");

//Task
Route::get("projects/{project:id}/tasks", [TaskController::class, "index"])->middleware("auth:sanctum");
Route::get("projects/{project}/tasks/{task}", [TaskController::class, "show"])->middleware("auth:sanctum");
Route::post("projects/{project:id}/tasks", [TaskController::class, "store"])->middleware("auth:sanctum");
Route::put("projects/{project}/tasks/{task}", [TaskController::class, "update"])->middleware("auth:sanctum");
Route::delete("projects/{project:id}/tasks/{task}", [TaskController::class, "destroy"])->middleware("auth:sanctum");

//Task Comment
Route::get("tasks/{task}/comments", [TaskCommentController::class, "index"])->middleware("auth:sanctum");
Route::post("tasks/{task}/comments", [TaskCommentController::class, "store"])->middleware("auth:sanctum");
Route::put("tasks/{task:id}/comments/{comment:id}", [TaskCommentController::class, "update"])->middleware("auth:sanctum");
Route::delete("tasks/{task:id}/comments/{comment:id}", [TaskCommentController::class, "destroy"])->middleware("auth:sanctum");

// Task Files
Route::get("tasks/{task}/files", [TaskFileController::class, "index"])->middleware("auth:sanctum");
Route::post("tasks/{task}/files", [TaskFileController::class, "store"])->middleware("auth:sanctum");
Route::get("tasks/{task:id}/files/{file:id}", [TaskFileController::class, "show"])->middleware("auth:sanctum");
Route::put("tasks/{task:id}/files/{file:id}", [TaskFileController::class, "update"])->middleware("auth:sanctum");
Route::delete("tasks/{task:id}/files/{file:id}", [TaskFileController::class, "destroy"])->middleware("auth:sanctum");

//Project Roles
Route::get('projects/{project:id}/member/', [ProjectRolesController::class, "index"])->middleware("auth:sanctum");
Route::post('projects/{project:id}/member/', [ProjectRolesController::class, "store"])->middleware("auth:sanctum");
Route::put('projects/{project:id}/member/{user:id}', [ProjectRolesController::class, "update"])->middleware("auth:sanctum");
Route::delete('projects/{project:id}/member/{user:id}', [ProjectRolesController::class, "destroy"])->middleware("auth:sanctum");

//Fitur gak guna
// Route::get("tasks/{task}/comments/{comment}", [TaskCommentController::class, "show"])->middleware("auth:sanctum");
// Route::resource("tasks", TaskController::class)->middleware("auth:sanctum");
// Route::resource('task-comments', TaskCommentController::class)->middleware('auth:sanctum');
